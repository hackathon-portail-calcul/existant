## Générer les fiches détails

À chaque modification / ajout / suppression de JSON des platforms, il est nécessaire de générer des fichiers markdowns pour chacun de ces JSON.

Le script `build-platforms.sh` sert à parcourir l'ensemble des fichiers et exécute les commande Hugo pour générer les markdowns.

```
./script/build-platforms.sh
```

## Installer les dépendances JS

```
cd assets/js
npm ci
```

## Build Hugo

```
hugo
```

## Développement

```
hugo server --disableFastRender
```

### Génération des JSON pour la liste des infrastructures et les options du formulaire

Pour l'OAD, 2 fichiers JSON sont nécessaires à la recherche. Ils sont générés au moment du build et chargés côté client en asynchrone.

Ces JSON sont générés par Hugo, pour celà, nous avons 2 fichiers `content/platforms.md` et `content/oad-options.md` qui servent à indiquer à Hugo que l'on souhaite générer des fichiers JSON.

Chacun de ces `.md` ont un layout correspondant :

- `layout/_default/platforms.json`
- `layout/_default/oad-options.json`

Ces layout nous permettent de structurer les données comme nous le voulons grâce aux fonctions Hugo et le templating Go. Quelques liens utiles :

- [Go templating](https://golang.org/pkg/text/template/)
- [La fonction Scratch](https://gohugo.io/functions/scratch/)
- [La fonction range](https://gohugo.io/functions/range/)
- [La fonction index](https://gohugo.io/functions/index-function/)


## Démo

URL temporaire / démo : http://pcs-oad.surge.sh/ 

## Pre-prod

(via Gitlab Pages)

url : https://ingenum.pages.mia.inra.fr/pcs

Ropo alexandra : https://forgemia.inra.fr/alexandra.janin/pcs

## Fichiers JSON

https://ingenum.pages.mia.inra.fr/pcs/platforms/index.json

https://ingenum.pages.mia.inra.fr/pcs/oad-options/index.json


