# Partie OAD

## La carte

Pour la carte on utilise __leaflet, leaflet.markercluster, vue2-leaflet, vue2-leaflet-markercluster__.

>  Il y a un bug avec le module __vue2-leaflet-markercluster__.  Impossible de la charger sans erreur, en mode standalone, module, bundle, bundle-module il y a tjrs une erreur.

le formulaire oad utilise vuejs et des plugins. Pour brancher facilement la carte on a utiliser le module vue2-leafet qui permet de définir une carte en mode vuejs.

> Pour evité de nouveau bug avec leaflet, il faudra mieux utiliser les libs natives (leaflet et leaflet.markercluster) et pas le wrappe avec vueJS.

### config

Pour rendre disponible les images de leaflet dans le fichier __config.toml__, un module.mount est défini pour permettre la copie des fichiers dans public (en faite on les monte dans static qui est copié dans public).

Modification des fichiers :  
* [[assets/js/oad.js]]
* [[layout/_default/oad.html]]
* [[layout/partial/js.html]] <- ajout (génére un bundle de lib JS) : déactivé
* [[themes/hugo-whisper-theme//assets/scss/components/_map.scss]] (qui est inclu dans themes/hugo-whisper-theme//assets/scss/style.scss)

En commentaire dans les fichiers oad.html et .js il y a des différentes manieres d'inclure les libs JS (CDN, bundle, link app).  


### data

Ajout des coordonnées (des prefectures des départements) pour les platformes dans les fichiers data/platform/*.json .  
Le platform.schema.json est à jour.  
C'est juste un array de number, on pourrait pousser a utiliser un schema qui décrit des coordonnées.  

Genre:  
```json
"properties": {
    "latitude": {
      "type": "number",
      "minimum": -90,
      "maximum": 90
    },
    "longitude": {
      "type": "number",
      "minimum": -180,
      "maximum": 180
    }
}
```


