# PCS : Portail Calcul Scientifique

## Hackathon
Ce projet est un fork de l'original pour l'usage du Hackathon de confinement.

Chaque groupe de travail du hackathon aura le choix entre forker ou créer une branche.

Dans notre cas **{project_name}** est : **'existant'**

## Information

Le but du Portail Calcul Scientifique est de répondre à la question : où sont les ressources de calcul, sous quelles conditions y accède-t-on et quelles sont leurs caractéristiques.
URL : [https://hackathon-portail-calcul.pages.mia.inra.fr/existant/](https://hackathon-portail-calcul.pages.mia.inra.fr/existant/)

Il se décline en 2 parties :

- [Documentation](https://hackathon-portail-calcul.pages.mia.inra.fr/existant/docs) et glossaire permettant d'asseoir un vocabulaire commun et d’assimiler les notions indispensables à la compréhension du calcul scientifique.

- [Outil d’aide à la décision](https://hackathon-portail-calcul.pages.mia.inra.fr/existant//oad) dans le choix d’une ressource en fonction des besoins et du profil de l’utilisateur.

La page d’accueil regroupe des informations générales et les dernières nouvelles.

C’est un outil proposé par le pôle Num4Sci de la DipsSO - INRAE et s’inscrit au portefeuille des projets du Schéma Directeur du Numérique.

Il encourage le travail collaboratif pour sa maintenance et son enrichissement.

Ci-dessous une documentation décrivant les procédures de collaboration.

## Documentation technique hackathon

### Ajout et/ou modification des infrastructures

- Créer / modifier un fichier descriptif de plateforme (en JSON) dans `/{project_name}/data/platform`

	- Attention : le fichier de plateforme doit être conforme au schéma décrit dans `/{project_name}/data/platform.schema.json`

	- Il est possible de s’inspirer des autres plateformes existantes pour la création d’une nouvelle

- Après le commit et le push, l'intégration continue (CI) se lance automatiquement afin de vérifier la conformité des fichiers

- Suivre le déroulement du CI sur : [https://forgemia.inra.fr/hackathon-portail-calcul/{project_name}/pipelines](https://forgemia.inra.fr/hackathon-portail-calcul/{project_name}/pipelines)

- Voir le rendu sur [https://hackathon-portail-calcul.pages.mia.inra.fr/existant/](https://hackathon-portail-calcul.pages.mia.inra.fr/existant/)

- Quand tous les *stages* du CI ont réussi, faire une merge request si vous avez forké ou créé une branche

> Bien veiller à ce que le **Target branch** soit `hackathon-portail-calcul/existant master`.

### Ajout/modification dans la partie documentation
Les fichiers de contenus sont dans le répertoire /content/docs/

Le contenu est écrit en Markdown et transformé en HTML automatiquement par l'intégration continue (CI) de gitlab à chaque 'commit'
