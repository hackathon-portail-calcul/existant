#!/bin/bash

# Clear existing platforms
if [ -d content/platform ]; then
    echo "Clearing platfrom contents"
    rm content/platform/*
else
    echo "Platform directory doesn't exist ... Exiting"
    exit 5
fi
# Generate platforms from JSON
for f in $(find ./data/platform -iname "*.json" -type f)
do
    echo "Create platform file $(basename $f .json)"
    hugo new platform/"$(basename $f .json)"".md"
done
