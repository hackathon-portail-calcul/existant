// @ts-nocheck
/*
* Sert à filtrer la recherche de texte libre.
* On recherche tous les mots du champs input
* return `true` si au moins 1 mot trouvé
*/
const filterText = (query, list) => !query || query === '' ||
query.toLowerCase().split(' ').filter(word => word !== ' ').find((word) =>
  list.split(' ').filter(listWord => listWord.indexOf(word) !== -1).length
)

/*
* Filtrer par tag
*/
const filterTag = (tags, current) => !tags.length || (Array.isArray(tags) ? tags.find(tag => current === tag) : tags === current)

/*
* Filtrer par slider
*/
const filterSlider = (range, current) => current >= range[0] && current <= range[1]

/*
* Filtrer par comparaison, utilisé notamment par le filtre bande passante
* lt: less than
* eq: equal
* gt: greater than
*/
const filterCompare = (compareWith, current) => {
  return !compareWith || compareWith === null ||
    (compareWith.value && compareWith.value[0] === 'eq'
      ? +current === +compareWith.value[1]
    : compareWith.value[0] === 'lt'
      ? +current <= +compareWith.value[1]
    : compareWith.value[0] === 'gt'
      ? +current >= +compareWith.value[1]
      : false)
}

const vuesearch = new Vue({
  el: '#oad-platforms',
  delimiters: ['${', '}'], // Syntaxe personnalisé pour éviter les conflits avec le templating Go
  components: {
    VueSlider: window['vue-slider-component'],
    VueMultiselect: VueMultiselect.Multiselect,
    VuePaginate: VuejsPaginate,
    'l-map': window.Vue2Leaflet.LMap,
    'l-tile-layer': window.Vue2Leaflet.LTileLayer,
    'l-marker': window.Vue2Leaflet.LMarker,
    'l-popup': window.Vue2Leaflet.LPopup,
    'l-tooltip': window.Vue2Leaflet.LTooltip,
    'l-icon': window.Vue2Leaflet.LIcon,
    'l-marker-cluster': window.Vue2LeafletMarkercluster
  },
  data: {
    model: {
      text: '',
      location: [],
      institute: [],
      storageType: [],
      totalStorage: [],
      totalCoreNumber: [],
      gpu: 'all',
      networkBandwith: undefined
    },
    db: [],
    dotOptions: [
      { tooltip: 'always' },
      { tooltip: 'always' }
    ],
    options: {
      departments: [],
      storageType: [],
      institutes: [],
      networkBandwith: [],
      storageSizeRange: [0, 10],
      coreNumberRange: [0, 10]
    },
    resultsPerPage: 10,
    currentPageIndex: 0,
    mapoad: {
      url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      zoom: 6,
      center: [46.71109, 1.7191036],
      bounds: null,
      markers: [],
      icon: L.icon({
        iconUrl: '../images/datacloud.svg',
        iconSize: [32, 37],
        iconAnchor: [16, 37]
      }),
      clusterOptions: [{maxClusterRadius:1000}, {disableClusteringAtZoom:11}]
    }
  },
  computed: {
    /**
     * S'applique à chaque modification du model (du formulaire)
     */
    filteredResults () {
      return this.db
        .filter(item =>
          // Filtre par localisation
          filterTag(this.model.location, item.location)
          // Filtre par institut
          && filterTag(this.model.institute, item.institute)
          // Filtre par type de stockage
          && (!this.model.storageType.length || this.model.storageType.find(type => item.storageType.indexOf(type) !== -1) )
          // Filtre par capacité de stockage
          && filterSlider(this.model.totalStorage, item.totalStorage)
          // Filtre par GPU
          && (this.model.gpu === 'all' || this.model.gpu.toString() === item.GPU.toString())
          // Filtre par nombre de cœurs
          && filterSlider(this.model.totalCoreNumber, item.totalCoreNumber)
          // Filtre par bande passante
          && filterCompare(this.model.networkBandwith, item.networkBandwith)
          // Filtrer par recherche libre
          && filterText(this.model.text, item.text)
        )
    },
    /**
     * Retourne la liste des résultats triés de la page courrante
     */
    currentPage() {
      return this.filteredResults.slice(this.currentPageIndex * this.resultsPerPage, this.currentPageIndex * this.resultsPerPage + this.resultsPerPage)
    },
    /**
     * Retourne le nombre de page et le nombre total de résultats (une fois filtré)
     */
    pagination () {
      return {
        pageCount: this.filteredResults.length / this.resultsPerPage,
        totalResults: this.filteredResults.length
      }
    }
  },
  mounted () {
    // Fetch les options du formulaire
    axios.get('/existant/oad-options/index.json')
    .then(response => {
        const queryString = tinyQuerystring.parse(window.location.hash.slice(1))
        // On ajoute les paramètres url au model de donnée existant
        const options = response.data
        this.options = {
          ...options,
          storageSizeRange: options.storageSizeRange.map(number => +number),
          coreNumberRange: options.coreNumberRange.map(number => +number)
        };

        Vue.nextTick(() => {
          // On attend que la modification des options soit effective
          this.model = {
            ...this.model,
            ...queryString,
            // On s'assure que les range soit bien de type number
            totalStorage: queryString.totalStorage ? queryString.totalStorage.map(number => +number) : this.options.storageSizeRange,
            totalCoreNumber: queryString.totalCoreNumber ? queryString.totalCoreNumber.map(number => +number) : this.options.coreNumberRange
          }
        });
      })
      .catch(error => {
      });
    // Fetch les données des infras
    axios.get('/existant/platforms/index.json')
      .then(response => {
        this.db = response.data.results;
      })
      .catch(error => {
      });
    window.setTimeout(this.addMarker, 2000);  
  },
  methods: {
    pageChange(pageNum) {
      this.currentPageIndex = pageNum - 1
    },
    /*
    * Ajoute les paramètres à l'url
    */
    addToUrl: (key, value) => {
      // Ajoute les paramètre dans l'url
      const params = { ...tinyQuerystring.parse(window.location.hash.slice(1)) }
      if (value) {
        params[key] = value
      } else {
        delete params[key]
      }
      window.location.hash = tinyQuerystring.stringify(params)
    },
    zoomUpdated (zoom) {
      //this.$refs.oadmap.mapObject.invalidateSize();
      this.mapoad.zoom = zoom;
    },
    centerUpdated (center) {
      this.mapoad.center = center;
    },
    boundsUpdated (bounds) {
      this.mapoad.bounds = bounds;
    },
    makePopup (elt) {
      return '<div class="map-popup"><div class="map-popup__title"><a href="'+elt.url+window.location.hash+'">'+elt.title+'</a><br/></div><b>'+elt.institute+
      '</b><br/><b>'+elt.totalStorage+'</b><br/><b>'+elt.storageType+'</b><br/><b>'+elt.networkBandwith+
      '</b><br/><b>'+elt.GPU+'</b><br/><b>'+elt.totalCoreNumber+'</b><br/></div>'
    },
    makeTooltip (elt) {
      return "<div><b>"+elt.title+"</b><br/><b>"+elt.institute+"</b><br/></div>"
    }
  }
});
