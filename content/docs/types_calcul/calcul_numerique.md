---
title: "Calcul numérique"
anchor: "calcul_numerique"
weight: 10
---
Le calcul numérique est une discipline qui traite de la conception, l’analyse et l’implémentation d’algorithmes pour la résolution numérique des problèmes mathématiques qui proviennent de la modélisation des phénomènes réels. On entend souvent par calcul numérique un ensemble de calculs qui sont réalisés sur un système informatique (ou ordinateur).
