---
title: "Ajouter une nouvelle infrastructure" 
anchor: "ajout_infrastructure"
weight: 11
---

## Étape 1
* Se connecter sur la forge MIA
* Cloner le projet PCS

## Étape 2
* Voir le fichier de Schéma JSON pour savoir ce qu'on doit mettre
* Créer notre fichier de description

## Étape 3
* Commiter
* Faire valider notre commit

## Étape 4
* Admirer le résultat
