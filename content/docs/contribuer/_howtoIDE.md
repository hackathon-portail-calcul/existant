---
title: "Comment rédiger (techniquement) la documentation via WebIDE"
anchor: "howto_redigerdoc"
weight: 10
---

# Concept
Les fichiers de la partie documentation sont dans le répertoire "content/docs" dans le dépôt de la forge (https://forgemia.inra.fr/hackathon-portail-calcul/existant/-/tree/master/content/docs).
Les sections principales sont matérialisées par un répertoire (par exemple definitions ou vocabulaire).
La description, le paramétrage et le contenu par défaut d'une section se trouve dans un fichier nommé par convention "_index.md".
On peut créer des sous-sections en créant des fichiers markdown (xxx.md) dans le répertoire.
Les fichiers sont au format markdown et acceptent également les balises HTML.
3 éléments sont importants pour chaque fichier : 
* title: "titre de la section" (ce qui sera affiché sur le rendu).
* anchor: "nom de l'ancre de la section" --> définir l'ancre (nom de la sous-section dans le système de fichiers; sans majuscules ni espaces ni accents ?)
* weight: "nombre" (poids donné à cette section par rapports aux autres du même répertoire, plus c'est grand, plus la section sera en bas)
Penser à l'ordre en amont...
---
Exemple tiré de : ajout_infra.md (dans : docs/contribuer/)

title: "Ajouter une nouvelle infrastructure"

anchor: "ajout_infrastructure"

weight: 11

---

# Howto

Pour créer du contenu de doc :

    Aller là : https://forgemia.inra.fr/hackathon-portail-calcul

    Aller dans le répertoire doc https://forgemia.inra.fr/hackathon-portail-calcul/existant/-/tree/master/content/docs (cliquer sur PCS, created 4 weeks ago, puis naviguer jusqu'à docs)

    Cliquer sur le bouton "Web IDE"

    Pour créer des dossiers (repository) ou fichiers (file) amener sa souris sur le dossier concerné (e.g. contribuer) et cliquer sur la flèche/chevron bas (après les trois points).

    Créer un répertoire représentant une section (si nécessaire; new directory) ou un fichier (i.e. sous-section; new file) 

    Créer un fichier _index.md à l'intérieur d'un nouveau répertoire (obligatoire) puis autant de fichiers .md que de sous-sections.

    * Pour _index.md copier un modèle d'index (adapter title, anchor et weight).

    * Pour des définitions de base copier le modèle "def_base" (à créer)

    * Pour une page de doc copier le modèle "doc_page" (à créer)

    Quand on a fini de créer le contenu, on clique sur commit (bouton bleu en bas à gauche), en mettant un message bien explicite dans la cellule juste au dessus pour expliquer ce qu'on a fait.

    Le moteur d'intégration continue s'occupe de la création des pages définitives et du déploiement automatiquement; il faut donc être patient !!


Pour vérifier, le rendu se voit là : https://hackathon-portail-calcul.pages.mia.inra.fr/existant/docs/

# Conseils

    Ne pas commiter trop souvent pour ne pas perdre trop de temps en intégration

    Prendre soin de vérifier l'état du pipeline pour s'assurer qu'il n'y a pas de soucis dans l'intégration et le déploiement

    S'inspirer des fichiers existants si on débute

# A faire

    Valider le mode opératoire
    
    Créer les fiches modèles dans la partie contribuer 

    Se répartir les rubriques à créer pour commencer à positionner l'arborescence
