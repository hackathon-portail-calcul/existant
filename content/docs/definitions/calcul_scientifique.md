---
title: "Calcul scientifique" 
anchor: "calcul_scientifique"
weight: 11
---

La terminologie calcul scientifique désigne tout calcul à l’usage de la science. _[1](#ref1)_

Le calcul scientifique est une discipline aux contours pas toujours franchement définis, mais qui regroupe un ensemble de champs mathématiques et informatiques permettant la simulation numérique des phénomènes de la physique, chimie, biologie, et sciences appliquées en général.

La simulation consiste à reproduire par le calcul le fonctionnement d’un système dont on connaît les principes de fonctionnement en général dans un souci d'économie ou parce que l'experimentation est impossible.

La modélisation consiste à approcher de façon simplifiée par le calcul, le fonctionnement de systèmes complexes ou d'évaluer les conséquences de théories scientifiques. La simulation numérique peut servir à valider ou à exploiter ces modèles.

