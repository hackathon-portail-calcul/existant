---
title: "Documentation"
anchor: "documentation"
weight: 10
---
{{% block warn %}}
Maquette de documentation. En cour de rédaction - le contenu peut être inexact ou incomplet
{{% /block %}}


Le but de cette documentation est de vous informer sur le calcul scientifique.

De trouver ici les bases du vocabulaire des définitions et des exemples concrets pour comprendre correctement les besoins et différences. Vous pourrez ainsi mieux vous orienter dans vos recherches, comprendre et exprimer vos besoins.
