---
title: "GPU"
anchor: "gpu"
weight: 14
---
Basé sur le circuit intégré des cartes graphiques qui gère l’affichage, le calcul sur GPU permet de paralléliser les tâches nécessitant peu de mémoire et offre un maximum de performances en accélérant les portions de code les plus fragmentables en ressources de calcul, le reste de l’application restant affecté au CPU. Avantage : coût matériel modique, traitement accéléré, faible consommation énergétique. _[1](#ref1)_

La combinaison de la technologie NVIDIA SLI permet à plusieurs GPU de fonctionner sur un seul PC. D’abord conçu pour les jeux graphiques les plus gourmands, la technologie des accélérateurs GPU a rapidement trouvé un champs d’applications dans le domaine du calcul intensif à faible coût.
