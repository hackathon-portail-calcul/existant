---
title: "Workflow"
anchor: "workflow"
weight: 20
---
Littéralement : chaîne de traitement. Cela désigne l’ensemble des opérations, de calcul qu’il faut effectuer pour arriver à un résultat souhaité. Et entré d’un workflow l’information est “brute”, l’exécution du workflow permet d’atteindre un résultat fini.
