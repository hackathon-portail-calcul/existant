---
title: "Coeurs"
anchor: "coeurs"
weight: 10
---
Au même titre que le coeur est un organe prépondérant dans notre organisme, un “coeur” physique sur un microprocesseur (CPU) est un ensemble de circuits capables capable de traiter les instructions qui vous permettent d’exécuter des programmes de façon autonome. Dans un cluster de calcul le nombre de coeurs disponible est une information importante surtout dans le cas ou votre code utilise la parallélisation.

{{% block note %}}
Ne pas confondre coeur physique et coeur logique (... explication)
{{% /block %}}
