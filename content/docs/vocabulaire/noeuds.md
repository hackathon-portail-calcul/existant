---
title: "Noeuds"
anchor: "noeuds"
weight: 12
---
Un noeud de calcul est une unité d’un cluster de calcul.
Les plateformes de calcul et mésocentre indique souvent l’nombre de noeud disponible pour préciser à l’utilisateur les ressources réservables. 
