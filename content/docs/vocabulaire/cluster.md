---
title: "Cluster"
anchor: "cluster"
weight: 13
---
C’est un assemblage d’ordinateurs de noeuds sur un même site connectés entre eux via des interconnexions rapides, partageant un espace disque et étant piloté par un distributeur de tâche commun. Le principe consiste à distribuer des tâches en parallèle sur les différents éléments (ou noeuds) qui composent le Cluster. Synonyme : grappe (ferme) de calcul.
