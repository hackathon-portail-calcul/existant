---
title: "Queue"
anchor: "queue"
weight: 17
---
C’est la file d'attente avant le traitement de son calcul.
Au niveau cluster de calcul, différents systèmes permettent de soumettre un “job” dans une queue.C’est aussi un élément pour diriger les utilisateurs : certaines queues sont prioritaire sur d’autre au sein d’un environnement de calcul ou donnent accès à différentes architectures.
