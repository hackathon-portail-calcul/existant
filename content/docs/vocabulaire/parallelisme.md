---
title: "Parallélisme"
anchor: "prallelisme"
weight: 18
---
Voir aussi Tache. Le besoin de paralléliser un code pour augmenter la vitesse d'exécution de ce code est une technique souvent utilisées. Certaines opérations sont facilement parallélisable, d’autre ne le sont pas : c’est à voir selon le contexte et les besoins de chacun. 
