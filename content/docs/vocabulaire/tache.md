---
title: "Tâche"
anchor: "tache"
weight: 16
---
Peut être considéré comme un synonyme de processus, ou d’un job.
C’est une unité d’exécution, de travail. Un système multitâche est une système permettant de gérer plusieurs tâches en parallèle, d’exécuter plusieur processus simultanément. L’ensemble des système d’exploitation moderne sont multitâche et dans une architecture de calcul pour accélérer la vitesse de traitement il est souvent utile d’exécuter un code parallélisable.
