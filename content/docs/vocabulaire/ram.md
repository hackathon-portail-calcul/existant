---
title: "RAM"
anchor: "ram"
weight: 15
---
Mémoire vive utilisable dans une système informatique. Dans le cadre du HPC, c’est souvent l’ensemble de la mémoire utilisable qui est indiqué. Certain type de calcul étant très gourmand en mémoire il est important d’avoir cette information. Au niveau des cluster de calcul la mémoire est partagé entre les noeuds pour former un pool important.
